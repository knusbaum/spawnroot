#include "common.h"

#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <unistd.h>
#include <dirent.h>

#define INITIAL_BINDDIR_SIZE 10

void * safemalloc(size_t size)
{
    void * newptr = malloc(size);
    if (newptr == NULL)
    {
        fprintf(stderr, "Out of memory, or allocation error.\n");
        exit(-1);
    }
    
    return newptr;
}

void * saferealloc(void * ptr, size_t size)
{
    void * newptr = realloc(ptr, size);
    if(newptr == NULL)
    {
        fprintf(stderr, "Out of memory, or allocation error.\n");
        exit(-1);
    }
    
    return newptr;
    
}

struct passwd * get_user(const char * username)
{
    struct passwd * user = getpwnam(username);
    if(user == NULL)
        return NULL;
    return user;
}

bool set_user(struct passwd * user)
{
    if(setgid(user->pw_gid) == -1)
        return false;

    if(setuid(user->pw_uid) == -1)
        return false;
    
    return true;
}

setup_vals get_options(int argc, char *argv[])
{
    setup_vals v;
    unsigned int current_dirs = 0;
    v.bind_dirs = safemalloc(sizeof(char *) * INITIAL_BINDDIR_SIZE);
    v.dircount = INITIAL_BINDDIR_SIZE;
    v.newroot = NULL;
    v.newuser = NULL;
    v.keep_oldroot = false;
    v.superuser = false;
    v.keep_proc = false;
    v.bound_etc = false;
    
    int c;
    while((c = getopt(argc, argv, "kn:b:c:u:sp")) != -1)
    {
        if(c == 'k')
        {
//            printf("Found argument 'a'\n");
            v.keep_oldroot = true;
        }
        else if(c == 'n')
        {
//            printf("Found argument 'n' with value %s\n", optarg);
            v.newroot = safemalloc( sizeof(char) * (strlen(optarg) + 1));
            strcpy(v.newroot, optarg);
        }
        else if(c == 'b')
        {
//            printf("Found argument 'b' with value %s\n", optarg);
            if (current_dirs == v.dircount)
            {
                v.dircount *= 2;
                v.bind_dirs = saferealloc(v.bind_dirs, sizeof (char *) * v.dircount);
            }
//            printf("Grabbing space for argument of size %u\n", strlen(optarg)+1);
            v.bind_dirs[current_dirs] = safemalloc(sizeof(char) * (strlen(optarg)+1));
            strcpy(v.bind_dirs[current_dirs], optarg);
            current_dirs++;
        }
        else if(c == 'u')
        {
            v.newuser = safemalloc( sizeof(char) * (strlen(optarg) + 1));
            strcpy(v.newuser, optarg);
        }
        else if(c == 's')
        {
            v.superuser = true;
        }
        else if(c == 'p')
        {
            v.keep_proc = true;
        }
    }
    v.dircount = current_dirs;
    
    return v;
}

bool is_valid_setup(setup_vals const * setup)
{
    if (setup->newroot == NULL)
    {
        fprintf(stderr, "[ERROR]: No new root selected.\n");
        return false;
    }
    else if(setup->newuser == NULL && !setup->superuser)
    {
        fprintf(stderr, "[ERROR]: Either specify a user, or use -s to run as root.\n");
        return false;
    }
    
    return true;
}

bool dir_exists(char const * filename)
{
    DIR * d = opendir(filename);
    if (!d)
        return false;
   
    closedir(d);
    return true;
}
