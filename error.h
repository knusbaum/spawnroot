#ifndef ERROR_H
#define ERROR_H
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>

#define LIBMNT_ALLOC_FAIL 1
#define LIBMNT_PARSE_FAIL 2
#define LIBMNT_SOFT_FAIL 3

extern char * binname;

// fatal(int status, bool perror, const char *fmt, ...)
#define fatal(...) _error(true, __VA_ARGS__)
// void warn(bool perror, const char * fmt, ...)
#define warn(...) _error(false, 0, __VA_ARGS__)

static inline __attribute__ ((format(printf, 4, 5))) void _error(bool doexit, int status, bool perror, const char *fmt, ...)
{
    fprintf(stderr, "[%s]: ", binname);
    if(fmt != NULL)
    {
        va_list arg;
        va_start(arg, fmt);
        vfprintf(stderr, fmt, arg);
        va_end(arg);
        if(perror)
            fprintf(stderr, ": ");
    }
    if(perror)
        fprintf(stderr, "%m");
    fprintf(stderr, "\n");
    if(doexit)
        exit(status);
}


#endif
