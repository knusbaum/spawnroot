#include "umount.h"

#include "error.h"
#include <sys/mount.h>
#include <libmount/libmount.h>

#define FSTYPES "noproc,nodevfs,nodevpts,nosysfs,norpc_pipefs,nonfsd"
#define PROC_MOUNTS_PATH "/proc/self/mountinfo"

// May have to change this eventually. 
// table_parser_errcb may need to return >0 in the future to ingore the invalid line
// when used in mnt_table_set_parser_errcb()
static int table_parser_errcb(struct libmnt_table *tb __attribute__((__unused__)),
                              const char *filename, int line)
{
    if(filename)
        warn(false, "Failed to parse %s line %d. Ignoring...", filename, line);
    return 0;
}


static int umount_fs_if_mounted(struct libmnt_context *cxt, const char *spec)
{
    int rc;
    struct libmnt_fs *fs;

    rc = mnt_context_find_umount_fs(cxt, spec, &fs);
    if(rc == 1)
    {
        rc == UMOUNT_SUCCESS;
        mnt_reset_context(cxt);
    }
    else if(rc < 0)
    {
        rc = UMOUNT_FAIL;
        mnt_reset_context(cxt);
    }
    else
    {
        spec = mnt_fs_get_target(fs);
        if(mnt_context_set_target(cxt, spec))
            fatal(LIBMNT_SOFT_FAIL, false, "Failed to set umount target");
        rc = mnt_context_umount(cxt);
        mnt_reset_context(cxt);
        rc = UMOUNT_SUCCESS;
    }
    return rc;
}

static int umount_do_recurse(struct libmnt_context *cxt, struct libmnt_table *tb,
                             struct libmnt_fs *fs)
{
    int rc;
    struct libmnt_fs * child;
    struct libmnt_iter *itr = mnt_new_iter(MNT_ITER_BACKWARD);

    if(!itr)
        fatal(LIBMNT_ALLOC_FAIL, false, "Failed to allocate iterator.");

    for(;;)
    {
        rc = mnt_table_next_child_fs(tb, itr, fs, &child);
        if(rc < 0) {
            fatal(LIBMNT_SOFT_FAIL, false, 
                  "Failed to get child filesystem of %s", mnt_fs_get_target(fs));
        }
        else if(rc == 1)
        {
            rc = umount_fs_if_mounted(cxt, mnt_fs_get_target(fs));
            break;
        }
        else
        {
            rc = umount_do_recurse(cxt, tb, child);
            if(rc != UMOUNT_SUCCESS)
                break;
        }
        
    }


    mnt_free_iter(itr);
    return rc;
}

static struct libmnt_table *new_mountinfo(struct libmnt_context *cxt)
{
    struct libmnt_table *tb = mnt_new_table();
	if (!tb)
            fatal(LIBMNT_SOFT_FAIL, false, "Failed to allocate table");

	mnt_table_set_parser_errcb(tb, table_parser_errcb);
	mnt_table_set_cache(tb, mnt_context_get_cache(cxt));

        
	if (mnt_table_parse_file(tb, PROC_MOUNTS_PATH))
            fatal(LIBMNT_PARSE_FAIL, true, "Failed to parse %s", PROC_MOUNTS_PATH);

        return tb;
}

int umount_recursive(const char *spec)
{
    int rc;
    mnt_init_debug(0);
    struct libmnt_context * cxt = mnt_new_context();
    if(!cxt)
        fatal(LIBMNT_ALLOC_FAIL, false, "Failed to allocate mount context.");
    mnt_context_set_tables_errcb(cxt, table_parser_errcb);

    if(mnt_context_is_restricted(cxt))
        fatal(-1, false, "Restricted permissions");

    struct libmnt_table *tb = new_mountinfo(cxt);

    mnt_context_disable_swapmatch(cxt, 1);

    struct libmnt_fs *fs = mnt_table_find_target(tb, spec, MNT_ITER_BACKWARD);
    if(fs)
        rc = umount_do_recurse(cxt, tb, fs);
    else
        fatal(LIBMNT_SOFT_FAIL, false, "Failed to umount %s", spec);
    
    mnt_unref_table(tb);
    return rc;
}
