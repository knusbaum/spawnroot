#define _GNU_SOURCE

#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

#include "error.h"
#include "umount.h"
#include "mountutils.h"
#include "common.h"

char * binname = "newspace";

void print_ids();
int newchild(void * arg);

int main(int argc, char *argv[])
{

    setup_vals s = get_options(argc, argv);

/*
    // DUMP SETUP VALS
    {
        printf("newroot: %s\n", s.newroot);
        printf("newuser: %s\n", s.newuser);
        printf("keep_oldroot: %s\n", s.keep_oldroot ? "true" : "false");
        printf("run as super: %s\n", s.superuser ? "true" : "false");
        int i;
        for(i = 0; i < s.dircount; i++)
        {
            printf("Bind Dir: %s\n", s.bind_dirs[i]);
        }
    }
*/  
    if(!is_valid_setup(&s))
        fatal(-1, false, "Invalid Setup");

    new_namespace(newchild, &s);

    int x = umount_recursive(s.newroot);
    if(x != UMOUNT_SUCCESS)
    {
        warn(true, "Failed to umount newroot");
    }
    
    wait(NULL);

}

int newchild(void * arg)
{

    setup_vals * s = arg;
    if (!setup_binds(s))
        fatal(-1, true, "Failed to bind.\n");

/*
    printf("\t|\n\t|\n\t|\n\t|\n\t|\n\t|\n\t|\n");
    // DUMP SETUP VALS
    {
        printf("newroot: %s\n", s->newroot);
        printf("newuser: %s\n", s->newuser);
        printf("keep_oldroot: %s\n", s->keep_oldroot ? "true" : "false");
        printf("run as super: %s\n", s->superuser ? "true" : "false");
        printf("bound etc: %s\n", s->bound_etc ? "true" : "false");
        int i;
        for(i = 0; i < s->dircount; i++)
        {
            printf("Bind Dir: %s\n", s->bind_dirs[i]);
        }
    }
*/
    make_root_rprivate("/proc/mounts");

    do_pivot(s);

    setup_proc();
    umount_oldroot(s);
    if(!s->keep_proc)
        umount_dir("/proc", false);

    

    struct passwd * user = NULL;
    if(!s->superuser)
    {
        user = get_user(s->newuser);
        if (user == NULL)
            fatal(-1, true, "Failed to getpwnam for user '%s'\n", s->newuser);
    }

    if(!s->bound_etc)
        umount_dir("/etc", true);

    char *const argv2[] = {NULL};
    char * envp[2];
    if(!s->superuser)
    {
        if (!set_user(user))
            fatal(-1, true, "Failed to setuid for user '%s'\n", s->newuser);
        char * home = safemalloc(sizeof(char) * (sizeof("HOME=/home/") + strlen(s->newuser)));
        char * user = safemalloc(sizeof(char) * (sizeof("USER=") + strlen(s->newuser)));
        
        strcpy(home, "HOME=/home/");
        strcat(home, s->newuser);
        strcpy(user, "USER=");
        strcat(user, s->newuser);
        envp[0] = home;
        envp[1] = user;
    }
    else
    {
        envp[0] = "HOME=/root";
        envp[1] = "USER=root";
    }
    

    execvpe("bash", argv2, envp);
    fatal(-1, true, "Failed to spawn process");
    

}

void print_ids(void)
{
    printf("euid: %d\tuid: %d\n", geteuid(), getuid());
    printf("egid: %d\tgid: %d\n", getegid(), getgid());
}
