#include "mountutils.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <asm/unistd.h>
#include <errno.h>
#include <pwd.h>
#include <string.h>
#include <sched.h>
#include <mntent.h>
#include "error.h"
#include "umount.h"
#include "common.h"



void new_namespace(int (*fn)(void *), setup_vals * s)
{
    char * stack = safemalloc(1024 * 1024 * sizeof(char));
    stack += (1024 * 1024 * sizeof(char));
    int x = clone(fn, (void *)stack, CLONE_NEWNS | CLONE_NEWPID | CLONE_NEWIPC | CLONE_NEWNET | CLONE_NEWUTS | CLONE_VFORK, s);
    if(! (x > 0))
    {
        fatal(-1, true, "Failed to spawn new namespace.");
    }
}

static void pivot_root(const char * newroot, const char * old_put)
{
    int x = syscall(__NR_pivot_root, newroot, old_put);
    if(x != 0)
        fatal(x, true, "Failed to pivot root");
}

void make_root_rprivate(const char * mounts_path)
{
    FILE * mounts = setmntent(mounts_path, "r");
    struct mntent * entry;
    while ((entry = getmntent(mounts)) != NULL)
    {
        if(strcmp(entry->mnt_dir, "/") == 0)
        {
            int x = mount(entry->mnt_dir, entry->mnt_dir, entry->mnt_type, MS_REC | MS_PRIVATE, entry->mnt_opts);
            if(x != 0)
            {
                char y[50];
                sprintf(y, "Failed to make root shared %s from %s\t", entry->mnt_fsname, entry->mnt_dir);
                perror(y);
                exit(x);
            }
            else
                printf("Success making root recursively private.\n");
            break;
        }
    }
    endmntent(mounts);
}

bool setup_binds(setup_vals * setup)
{
    printf("binds: %d\n", setup->dircount);
    bool ret = true;
    
    //Bind newdir
    int x = mount(setup->newroot, setup->newroot, NULL, MS_BIND, NULL);
    if (x != 0)
    {
        perror("Could not bind new root");
        ret = false;
    }
    
    for(int i = 0; i < setup->dircount; i++)
    {
        if(strcmp(setup->bind_dirs[i], "/etc") == 0)
            setup->bound_etc = true;
    }

    char ** bind_dirs = realloc(setup->bind_dirs, sizeof(char *) * (setup->dircount + 1));
    if(!bind_dirs)
        fatal(-1, true, "Failed to allocate for bind_dirs");

    if(!setup->bound_etc)
        setup->bind_dirs[setup->dircount++] = "/etc";

    for(int i = 0; i < setup->dircount; i++)
    {
        char * target = safemalloc(sizeof(char) * (strlen(setup->newroot) + 1 + strlen(setup->bind_dirs[i]) + 1));

        strcpy(target, setup->newroot);
        strcat(target, "/");
        strcat(target, setup->bind_dirs[i]);

        if(!dir_exists(target))
        {
            printf("Creating directory %s\n", target);
            mkdir(target, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
        }


        printf("Binding %s to %s\n", setup->bind_dirs[i], target);
        int x = mount(setup->bind_dirs[i], target, NULL, MS_BIND, NULL);
        if (x != 0)
        {
            perror("Bind Failed");
            ret = false;
        }
        else
            printf("Successfully bound %s\n", setup->bind_dirs[i]);
    }
    return ret;
}

void setup_proc()
{
    if(!dir_exists("/proc"))
        mkdir("/proc", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
     
    int x = mount(NULL, "/proc", "proc", 0, NULL);
    if(x != 0)
        fatal(-1, true, "mount proc failed");
}

void umount_dir(char const * path, bool keepdir)
{
        int x = umount(path);
        if(!x == 0)
            fatal(-1, true, "Failed to umount %s", path);
        if(!keepdir)
            rmdir(path);
}

void umount_oldroot(setup_vals const * s)
{
    if(!s->keep_oldroot)
    {
        printf("Umounting oldroot.\n");
        int x = umount_recursive("/oldroot"); 
        if (x != UMOUNT_SUCCESS)
            fatal(-1, true, "Failed to unmount old root");
        
        x = rmdir("/oldroot");
        if(x != 0)
            warn(true, "Failed to remove directory oldroot");
    }
}

void do_pivot(setup_vals *s)
{
    char * oldroot = safemalloc(sizeof(char) * (strlen(s->newroot) + 1 + strlen("oldroot") + 1));
    strcpy(oldroot, s->newroot);
    strcat(oldroot, "/");
    strcat(oldroot, "oldroot");

    if(!dir_exists(oldroot))
        mkdir(oldroot, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    pivot_root(s->newroot, oldroot);
    chdir("/");

}
