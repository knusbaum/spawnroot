#ifndef MOUNTUTILS_oH
#define MOUNTUTILS_H
#define _GNU_SOURCE

#include "common.h"

void new_namespace(int (*fn)(void *), setup_vals *s);
void make_root_rprivate(const char * mounts_path);
bool setup_binds(setup_vals * setup);
void do_pivot(setup_vals *s);
void setup_proc();
void umount_dir(char const * path, bool keepdir);
void umount_oldroot(setup_vals const * s);

#endif
