#define _GNU_SOURCE
#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
#include <stdio.h>


typedef struct {
    char ** bind_dirs;
    unsigned int dircount;
    char * newroot;
    char * newuser;
    char * newuser_dir;
    bool keep_oldroot;
    bool superuser;
    bool keep_proc;
    bool bound_etc;
} setup_vals;

void * safemalloc(size_t size);
void * saferealloc(void * ptr, size_t size);
struct passwd * get_user(const char * username);
bool set_user(struct passwd * user);
setup_vals get_options(int argc, char *argv[]);
bool is_valid_setup(setup_vals const *);
bool dir_exists(char const * filename);

#endif
