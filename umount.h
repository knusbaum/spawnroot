#ifndef UMOUNT_H
#define UMOUNT_H

int umount_recursive(const char *spec);
#define UMOUNT_SUCCESS 1
#define UMOUNT_FAIL 0

#endif
