CC = cc
CFLAGS = -pedantic -std=c99 -ggdb
LDFLAGS = -lmount 

OBJECTS = main.o\
	umount.o\
	mountutils.o\
	common.o

BINNAME = newspace

all: clean $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) -o $(BINNAME)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	-@rm *.o *~
